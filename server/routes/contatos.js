module.exports = function(app){

	var controller = app.controllers.contatos;

	app.route('/api/contatos')
			.get(controller.list)
			.post(controller.save);

	app.route('/api/contatos/:id')
			.get(controller.get)
			.delete(controller.delete);

}