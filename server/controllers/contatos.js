module.exports = function(app){
	var Contato = app.models.contato;
	var controller = {};

	controller.get = function(req, res){
		Contato.findById(req.params.id).exec().then(function(contato){
			res.json(contato);
		});
	}

	controller.list = function(req,res){
		Contato.find().populate('emergencia').exec().then(function(contatos){
			res.json(contatos);
		})
	}

	controller.save = function(req,res){
		var _id = req.body._id;

		req.body.emergencia = req.body.emergencia || undefined;

		if (!_id) {
			Contato.create(req.body).then(function(contato){
				res.json(contato);
			});
		} else {
			Contato.findByIdAndUpdate(_id, req.body).exec().then(function(contato){
				res.json(contato);
			});
		}
	}

	controller.delete = function(req,res){
		Contato.remove({"_id": req.params.id}).exec().then(function(){
			res.end();
		})
	}

	return controller;
}