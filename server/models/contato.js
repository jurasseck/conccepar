var mongoose = require('mongoose');

module.exports = function(){
	var contatoSchema = mongoose.Schema({
		nome: {type: String, default: '', required: true},
		email: {type: String, default: '', index: {unique : true}},
		telefones: [
			{type: String, default: ''}
		],
		emergencia : {
			type: mongoose.Schema.ObjectId,
			ref: 'Contato'
		},
		dataCriacao: {type: Date, default : Date.now} 
	});

	return mongoose.model('Contato',contatoSchema);
}