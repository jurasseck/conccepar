'use strict';

var app = angular.module('app',['ngRoute', 'ngResource']);

app.config(function($routeProvider){

	$routeProvider.when('/', {
		templateUrl: 'partials/list.html',
		controller: 'contatosController'
	}).when('/contatos',{
		redirectTo: '/'
	}).when('/contato/:id',{
		templateUrl: 'partials/form.html',
		controller: 'contatoController'
	}).when('/contato',{
		templateUrl: 'partials/form.html',
		controller: 'contatoController'

	}).otherwise({redirectTo: '/'});

});

app.controller('contatosController', function($scope, Contato){

	function buscarContatos(){
		Contato.query(function(contatos){
			$scope.contatos = contatos;
		});
	}

	$scope.remover = function(contato){
		Contato.delete(
			{id: contato._id},
			buscarContatos
		);
	}

	buscarContatos();
});

app.controller('contatoController', function($scope, Contato, $routeParams){

	if ($routeParams.id) {
		Contato.get({id: $routeParams.id}, function(contato){
			$scope.contato = contato;
		});
	} else {
		$scope.contato = new Contato();
	}

	Contato.query(function(contatos){
		$scope.contatos = contatos;
	});

	$scope.gravar = function(){
		$scope.contato.$save().then(function(){
			$scope.contato = new Contato();
		});
	}

	$scope.adicionarTelefone = function(){
		$scope.contato.telefones.push($scope.telefone);
		$scope.telefone = '';
	}

	$scope.removerTelefone = function(telefone){
		var idx = $scope.contato.telefones.indexOf(telefone);
		$scope.contato.telefones.splice(idx, 1);
	}

});

app.factory('Contato', function($resource){
	return $resource('/api/contatos/:id');
})